# WPCOM V.1.0.3 #

### What is this repository for? ###

This repository contains WPCOM theme that is used by https://www.wpcom.org.

### How do I get set up? ###

This repository ONLY contains the theme. For database and plugins, contact repo admin.

In this particular theme, following libraries are using:
-Grunt
-SaSS
-LiveReload

To start the local server, navigate to repo and run the following command:
grunt server

### Who do I talk to? ###

-Atakan Oz (atakan@wpcom.org) - Repo Admin

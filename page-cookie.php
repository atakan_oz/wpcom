<?php
/* Template Name: Cookie Policy */
/**
 * 	Front Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */

?>
<?php get_header('page'); ?>

<div id="about" class="content">
  <div class="container">
    <div class="row row-wrap">
      <div id="about-content" class="column column-75">
        <div class="row row-wrap">
          <div class="column column-100">
            <div class="about-section">
              <div class="about-section-title">
                <h5>Cookie Policy</h5>
              </div>
              <div class="about-content">

                <?php
                if( have_posts() ) :
              		while ( have_posts() ) :
              			the_post();

              			the_content();

              		endwhile; // End of the loop.
                endif;
            		?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php get_template_part( 'templates/components/side-nav'); ?>
    </div>
  </div>
  <?php get_template_part( 'templates/components/page-sections'); ?>
</div>
<?php get_footer(); ?>

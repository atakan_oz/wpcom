<?php
/* Template Name: Most Popular */
/**
 * 	Archive Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */

?>
<?php get_header('page'); ?>

<div class="content">
  <div class="container">
    <div class="row row-wrap">
      <!-- Sidebar Starts -->
      <?php get_template_part( 'templates/components/side-nav'); ?>
      <!-- Sidebar Ends -->
      <!-- Home Sections Starts -->
      <div id="archive" class="column column-75">
        <div class="row row-wrap">
          <div class="column column--section column-featured column-100">
            <div class="row row-wrap">
              <div class="column column-100">
                <div class="resource-loop">
                  <div class="section-title title--loop">
                    <h5>Most Popular WordPress Resources</h5>
                  </div>
                  <?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="8" offset="0" pause="true" posts_per_page="100" nested="true" post_type="resource" meta_key="_liked" meta_value="" orderby="meta_value_num" scroll="false" button_label="More Resources" button_loading_label="Loading Resources..."]'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Home Sections Ends -->
  <?php get_template_part( 'templates/components/page-sections'); ?>
</div>

<?php get_footer(); ?>

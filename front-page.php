<?php
/**
 * 	Front Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */

 // Crew Picks Args
 $crew_picks_args = array(
   'post_type' => 'resource',
	 'meta_key' => 'resource_order',
   	'orderby'=> 'meta_value_num',
   	'order' => 'DESC',
   'posts_per_page' => 10,
   'no_found_rows' => true,
   'post_status' => 'publish',
   'post__in' => array(1358, 1360, 1299, 1351, 1274, 1461, 1268, 1268, 1268)
 );

 // Home Plugins Args
 $home_plugins_args = array(
  'post_type' => 'resource',
 	'posts_per_page' => 5,
 	'no_found_rows' => true,
 	'post_status' => 'publish',
  'tax_query' => array(
   array(
       'taxonomy' => 'resource_category',                //(string) - Taxonomy.
       'field' => 'slug',                    //(string) - Select taxonomy term by ('id' or 'slug'
       'terms' => 'wordpress-tools',
       'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
       'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
     ),
   ),
 );

 // Home Plugins Args
 $home_hosting_args = array(
  'post_type' => 'resource',
 	'posts_per_page' => 5,
 	'no_found_rows' => true,
 	'post_status' => 'publish',
  'tax_query' => array(
   array(
       'taxonomy' => 'resource_category',                //(string) - Taxonomy.
       'field' => 'slug',                    //(string) - Select taxonomy term by ('id' or 'slug'
       'terms' => 'wordpress-hosting',
       'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
       'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
     ),
   ),
 );

 // Home Plugins Args
 $home_themes_args = array(
  'post_type' => 'resource',
 	'posts_per_page' => 7,
 	'no_found_rows' => true,
 	'post_status' => 'publish',
  'tax_query' => array(
   array(
       'taxonomy' => 'resource_category',                //(string) - Taxonomy.
       'field' => 'slug',                    //(string) - Select taxonomy term by ('id' or 'slug'
       'terms' => 'wordpress-themes',
       'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
       'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
     ),
   ),
 );

 $home_support_args = array(
  'post_type' => 'resource',
 	'posts_per_page' => 7,
 	'no_found_rows' => true,
 	'post_status' => 'publish',
  'tax_query' => array(
   array(
       'taxonomy' => 'resource_category',                //(string) - Taxonomy.
       'field' => 'slug',                    //(string) - Select taxonomy term by ('id' or 'slug'
       'terms' => 'wordpress-support',
       'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
       'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
     ),
   ),
 );

 $home_courses_args = array(
  'post_type' => 'resource',
 	'posts_per_page' => 7,
 	'no_found_rows' => true,
 	'post_status' => 'publish',
  'tax_query' => array(
   array(
       'taxonomy' => 'resource_category',                //(string) - Taxonomy.
       'field' => 'slug',                    //(string) - Select taxonomy term by ('id' or 'slug'
       'terms' => 'wordpress-courses',
       'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
       'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
     ),
   ),
 );

?>
<?php get_header("front"); ?>

<div class="content">
  <div class="row row-wrap">
    <div id="home-sections" class="featured-of-month column column-100">
      <div class="container">
        <div class="row row-wrap">
            <div id="home-featured" class="column column--section column-featured column-100">
            <div class="row row-wrap">
              <div class="column column-100">
                <div class="resource-loop loop--featured">
                  <div class="section-title title--loop title--featured">
                    <h5>Featured This Month</h5>
                    <p>Every month we carefully pick some of the best resources from our collection for you.</p>
                  </div>
                  <div class="column column-100 owl-carousel owl-theme owl-featured featured-style owl-loaded owl-drag">
                    <?php
                    $featured_query2 = new WP_Query($crew_picks_args);
                    if ($featured_query2->have_posts()) : ?>
                      <?php while ( $featured_query2->have_posts() ) : $featured_query2->the_post(); ?>
                        <?php get_template_part('/templates/posts/featured'); ?>
                      <?php endwhile;?>
                    <?php else : ?>
                    <?php endif; wp_reset_query();?>
                  </div>
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>most-popular" class="more more--home">Discover most popular resources <span class="icon icon_chevron"></span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Sidebar Ends -->
    <!-- Home Sections Starts -->
    <div id="home-sections" class="featured-collections column column-100">
      <div class="row row-wrap">
        <div id="home-featured" class="column column--section column-featured column-100">
          <div class="container">
            <div class="row row-wrap">
              <div class="column column-100">
                <div class="resource-loop loop--featured">
                  <div class="section-title title--loop title--featured">
                    <h5>Inspiring Categories</h5>
                    <p>We've extensively categorized every single resource for you to help you to find your resource much faster.</p>
                  </div>
                  <div class="column column-100 owl-carousel owl-theme cat-carousel featured-style">
                    <div class="cat-box" style="background-image: url('<?php echo get_template_directory_uri() ?>/dist/img/cat-advertising.jpg')">
                      <a class="cat-link" href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-tools/advertising-plugins"></a>
                    </div>
                    <div class="cat-box" style="background-image: url('<?php echo get_template_directory_uri() ?>/dist/img/cat-backup.jpg')">
                      <a class="cat-link" href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-tools/backup-plugins"></a>
                    </div>
                    <div class="cat-box" style="background-image: url('<?php echo get_template_directory_uri() ?>/dist/img/cat-speed.jpg')">
                      <a class="cat-link" href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-tools/performance-plugins"></a>
                    </div>
                    <div class="cat-box" style="background-image: url('<?php echo get_template_directory_uri() ?>/dist/img/cat-pagebuilders.jpg')">
                      <a class="cat-link" href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-tools/page-builders"></a>
                    </div>
                    <div class="cat-box" style="background-image: url('<?php echo get_template_directory_uri() ?>/dist/img/cat-forms.jpg')">
                      <a class="cat-link" href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-tools/forms-plugins"></a>
                    </div>
                    <div class="cat-box" style="background-image: url('<?php echo get_template_directory_uri() ?>/dist/img/cat-security.jpg')">
                      <a class="cat-link" href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-tools/security-plugins"></a>
                    </div>
                  </div>
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>most-recent" class="more more--home">Discover more categories <span class="icon icon_chevron"></span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="home-sections" class="recent-resources column column-100">
      <div class="container">
        <div class="row row-wrap">
          <div id="home-featured" class="column column--section column-featured column-100">
            <div class="row row-wrap">
              <div class="column column-100">
                <div class="resource-loop loop--featured">
                  <div class="section-title title--loop title--featured">
                    <h5>Latest Resources</h5>
                    <p>These resources are just added to our collection and waiting your attention.</p>
                    <ul id="tabs" class="nav-tabs">
                      <li class="active"><a href="#tab-tools">Tools</a></li>
                      <li><a href="#tab-hosting">Hosting</a></li>
                      <li><a href="#tab-themes">Themes</a></li>
                      <li><a href="#tab-support">Support</a></li>
                    </ul>
                  </div>
                  <div class="tab-content">
                    <div id="tab-tools" class="tab-pane active">
                      <div class="column column-100 owl-carousel owl-theme owl-featured featured-style owl-loaded owl-drag">
                        <?php
                        $featured_query2 = new WP_Query($home_plugins_args);
                        if ($featured_query2->have_posts()) : ?>
                          <?php while ( $featured_query2->have_posts() ) : $featured_query2->the_post(); ?>
                            <?php get_template_part('/templates/posts/featured'); ?>
                          <?php endwhile;?>
                        <?php else : ?>
                        <?php endif; wp_reset_query();?>
                      </div>
                      <a href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-tools/" class="more more--home">Discover more tools & plugins <span class="icon icon_chevron"></span></a>
                    </div>
                    <div id="tab-hosting" class="tab-pane">
                      <div class="column column-100 owl-carousel owl-theme owl-featured featured-style">
                        <?php
                        $featured_query3 = new WP_Query($home_hosting_args);
                        if ($featured_query3->have_posts()) : ?>
                          <?php while ( $featured_query3->have_posts() ) : $featured_query3->the_post(); ?>
                            <?php get_template_part('/templates/posts/featured'); ?>
                          <?php endwhile;?>
                        <?php else : ?>
                        <?php endif; wp_reset_query();?>
                      </div>
                      <a href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-hosting/" class="more more--home">Discover more hosting providers <span class="icon icon_chevron"></span></a>
                    </div>
                    <div id="tab-themes" class="tab-pane">
                      <div class="column column-100 owl-carousel owl-theme owl-featured featured-style">
                        <?php
                        $featured_query3 = new WP_Query($home_themes_args);
                        if ($featured_query3->have_posts()) : ?>
                          <?php while ( $featured_query3->have_posts() ) : $featured_query3->the_post(); ?>
                            <?php get_template_part('/templates/posts/featured'); ?>
                          <?php endwhile;?>
                        <?php else : ?>
                        <?php endif; wp_reset_query();?>
                      </div>
                      <a href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-themes/" class="more more--home">Discover more themes <span class="icon icon_chevron"></span></a>
                    </div>
                    <div id="tab-support" class="tab-pane">
                      <div class="column column-100 owl-carousel owl-theme owl-featured featured-style">
                        <?php
                        $featured_query3 = new WP_Query($home_support_args);
                        if ($featured_query3->have_posts()) : ?>
                          <?php while ( $featured_query3->have_posts() ) : $featured_query3->the_post(); ?>
                            <?php get_template_part('/templates/posts/featured'); ?>
                          <?php endwhile;?>
                        <?php else : ?>
                        <?php endif; wp_reset_query();?>
                      </div>
                      <a href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-support/" class="more more--home">Discover more support services <span class="icon icon_chevron"></span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="home-sections" class="recent-courses column column-100">
      <div class="row row-wrap">
        <div id="home-featured" class="column column--section column-featured column-100">
          <div class="container">
            <div class="row row-wrap">
              <div class="column column-100">
                <div class="resource-loop loop--featured">
                  <div class="section-title title--loop title--featured">
                    <h5>Learn WordPress</h5>
                    <p>With more than 25 percent of all websites in the world powered by WordPress, there has never been a better time to build an income and a business around it.</p>
                  </div>
                  <div class="column column-100 owl-carousel owl-theme owl-featured featured-style owl-loaded owl-drag">
                    <div class="resource-card" style="background-image: url('<?php echo get_template_directory_uri() ?>/dist/img/courses.jpg');">
                      <a class="resource-link" href="#">
                      </a>
                    </div>
                    <?php
                    $featured_query2 = new WP_Query($home_courses_args);
                    if ($featured_query2->have_posts()) : ?>
                      <?php while ( $featured_query2->have_posts() ) : $featured_query2->the_post(); ?>
                        <?php get_template_part('/templates/posts/featured'); ?>
                      <?php endwhile;?>
                    <?php else : ?>
                    <?php endif; wp_reset_query();?>
                  </div>
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-courses/" class="more more--home">Discover more courses <span class="icon icon_chevron"></span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Home Sections Ends -->
    <?php get_template_part( 'templates/components/page-sections'); ?>
  </div>
</div>

<script type=“application/ld+json”>
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "name": "WPCOM",
  "url": "http://www.wpcom.org",
  "potentialAction": {
    "@type": "SearchAction",
    "target": {
      "@type": "EntryPoint",
      "urlTemplate": "http://www.wpcom.org/?s={query}"
    },
    "query": "required"
  }
}
</script>

<?php get_footer(); ?>

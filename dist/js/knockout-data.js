// Knockout Modelling
// Knockout modelling is using for front-end development at WPCOM
// ---------------------------------------------------------------
// Overall viewmodel for this screen, along with initial state
function Resources() {
    var self = this;
    // Data for Featured Resource
    self.featuredResources = [
        { resName: "Kinsta", url: "url(dist/img/kinsta1.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
        { resName: "StudioPress", url: "url(dist/img/studiopress.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
        { resName: "Jupiter", url: "url(dist/img/jupiter.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
        { resName: "iThemes Security", url: "url(dist/img/ithemes.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
        { resName: "Gutenype", url: "url(dist/img/gutentype.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
    ];
    // Data for Featured Section
    self.featuredSection = [
      { resName: "Kinsta", url: "url(dist/img/kinsta1.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
      { resName: "StudioPress", url: "url(dist/img/studiopress.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
      { resName: "Jupiter", url: "url(dist/img/jupiter.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
      { resName: "iThemes Security", url: "url(dist/img/ithemes.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
      { resName: "Gutenype", url: "url(dist/img/gutentype.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
    ];
    self.sider = [
        { resName: "Gutenype", url: "url(dist/img/gutentype.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
        { resName: "StudioPress", url: "url(dist/img/studiopress.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
    ];
    // Data for Featured Loop
    self.categoryLoop = [
      { resName: "Kinsta", url: "url(dist/img/kinsta1.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
      { resName: "StudioPress", url: "url(dist/img/studiopress.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
      { resName: "Jupiter", url: "url(dist/img/jupiter.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
      { resName: "iThemes Security", url: "url(dist/img/ithemes.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
      { resName: "Gutenype", url: "url(dist/img/gutentype.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
      { resName: "Jupiter", url: "url(dist/img/jupiter.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
      { resName: "iThemes Security", url: "url(dist/img/ithemes.png)", resDesc: "The best managed WordPress hosting, powered by Google Cloud and LXD orchestrated Linux containers…" },
    ];
};

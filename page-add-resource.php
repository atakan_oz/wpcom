<?php
/* Template Name: Add Resource */
/**
 * 	Front Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */

?>

<?php get_header('page'); ?>

<div id="about" class="content">
  <div class="container">
    <div class="row row-wrap">
      <div id="about-content" class="column column-75">
        <div class="row row-wrap">
          <div class="column column-100">
            <div class="about-section">
              <div class="about-content about-page">
                <?php echo do_shortcode('[contact-form-7 id="241" title="Add Resource"]'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php get_template_part( 'templates/components/side-nav'); ?>
    </div>
  </div>
  <?php get_template_part( 'templates/components/page-sections'); ?>
</div>
<?php get_footer(); ?>

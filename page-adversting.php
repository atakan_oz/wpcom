<?php
/* Template Name: Adversting */
/**
 * 	Front Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */

?>
<?php get_header('page'); ?>

<div id="about" class="content">
  <div class="container">
    <div class="row row-wrap">
      <div id="about-content" class="column column-75">
        <div class="row row-wrap">
          <div class="column column-100">
            <div class="about-section">
              <div class="about-section-title">
                <h5>Advertising</h5>
              </div>
              <div class="about-content">
                <p>Currently, advertising statistics are not available because we've just launched our directory. That's why we are currently in a Beta version where we didn't release our exact advertising plans yet.</p>
                <p>At this point, there are few options that we are providing for experimental purposes. Reach us to learn more about them!</p>
                <?php echo do_shortcode('[contact-form-7 id="1554" title="Advertiser Request"]')?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php get_template_part( 'templates/components/side-nav'); ?>
    </div>
  </div>
  <?php get_template_part( 'templates/components/page-sections'); ?>
</div>
<?php get_footer(); ?>

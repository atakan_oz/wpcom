<?php
/**
 * 	Archive Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */


global $wp_query;
$cat = get_queried_object();
$term = $cat->term_id;
$parent_cat = get_term_parents_list( $term, 'resource_tag', $args = array('separator' => ''));

?>
<?php get_header('cat'); ?>

<?php
  $document_title = wp_get_document_title();
  $section_title = strtr($document_title, array("WPCOM"=>""));
 ?>

<div class="content">
  <div class="container">
    <div class="row row-wrap">
      <!-- Sidebar Starts -->
      <?php get_template_part( 'templates/components/side-nav'); ?>
      <!-- Sidebar Ends -->
      <!-- Home Sections Starts -->
      <div id="archive" class="column column-75">
        <div class="row row-wrap">
          <div class="column column--section column-featured column-100">
            <div class="row row-wrap">
              <div class="column column-100">
                <div class="resource-loop">
                  <div class="section-title title--loop">
                    <h2>Best <?php echo $section_title ?> (<?php echo $cat->count; ?>)</h2>
                  </div>
                  <?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="10" offset="0"  pause="true" posts_per_page="15" nested="true" post_type="resource" taxonomy="'. $cat->taxonomy .'" taxonomy_terms="'. $cat->name .'" taxonomy_operator="IN" meta_key="resource_order" meta_value="" meta_compare="IN" orderby="meta_value_num" scroll="false" button_label="More Resources" button_loading_label="Loading Resources..."]'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Home Sections Ends -->
  <?php get_template_part( 'templates/components/page-sections'); ?>
</div>

<?php get_footer(); ?>

<?php
/* Template Name: Most Recent */
/**
 * 	Archive Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */

?>
<?php get_header('page'); ?>

<div class="content">
  <div class="container">
    <div class="row row-wrap">
      <!-- Sidebar Starts -->
      <?php get_template_part( 'templates/components/side-nav'); ?>
      <!-- Sidebar Ends -->
      <!-- Home Sections Starts -->
      <div id="archive" class="column column-75">
        <div class="row row-wrap">
          <div class="column column--section column-featured column-100">
            <div class="row row-wrap">
              <div class="column column-100">
                <div class="resource-loop">
                  <div class="section-title title--loop">
                    <h5>Recently Added WordPress Resources</h5>
                  </div>
                  <?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="8" offset="0" pause="true" offset="0" posts_per_page="15" nested="true" post_type="resource" orderby="date" order="DESC" scroll="false" button_label="More Resources" button_loading_label="Loading Resources..."]'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Home Sections Ends -->
      <?php get_template_part( 'templates/components/page-sections'); ?>
    </div>
  </div>
</div>

<?php get_footer(); ?>

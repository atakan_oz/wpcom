<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wpcom
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<?php if ( ! function_exists( '_wp_render_title_tag' ) ) :
    function theme_slug_render_title() {
  ?>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php
    }
    add_action( 'wp_head', 'theme_slug_render_title' );
	endif;
	?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<script data-ad-client="ca-pub-4943041254401527" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">


<?php
if (get_queried_object()->parent == 234) {
	$post_fix = 'Plugins';
} elseif (get_queried_object()->term_id == 315 || get_queried_object()->term_id == 311) {
	$post_fix = 'Themes';
} else {
  $post_fix = "";
}
?>
<div class="global__nav">
	<div class="container">
		<div class="row row-wrap row-no-padding">
			<div class="column column-100 header-master">
				<div class="column">
					<div class="header__block header--sites">
						<ul>
							<li class="wpcom active"><a href="https://www.wpcom.org">WordPress</a></li>
							<li class="startupcom"><a data-micromodal-trigger="modal--startup" href="#">Startups</a></li>
							<li><a href="#" data-micromodal-trigger="modal--marketing">Marketing</a></li>
							<li><a href="#" data-micromodal-trigger="modal--seo">SEO</a></li>
						</ul>
					</div>
					<div class="header__block header--brand">
						<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri() ?>/dist/img/wpcom-logo-mobile.svg" alt="wpcom-logo"></a>
					</div>
					<div class="header__block header--account">
						<ul>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>report-resource">Report</a></li>
							<li class="submit"><a href="<?php echo esc_url( home_url( '/' ) ); ?>add-resource">Submit Resource</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="head">
	<header class="main__header">
		<div class="container">
			<div class="row row-wrap row-no-padding">
				<div class="column column-100 header-master">
					<div class="column">
						<div class="header__block header--menu">
							<a class="mobile-trigger" href="#menu"><i class="icon icon_menu"></i> Menu</a>
							<?php
							if(false === ($main_menu = get_transient('primary_menu'))) {
								ob_start();
		            wp_nav_menu(array(
		              'theme_location' =>  'header-nav',
		              'container'      =>  false,
		              'items_wrap'     =>  '<ul class="header-menu">%3$s</ul>',
		              'walker'         =>  new wpcom_main_nav(),
		              'depth'          =>  '2',
		            ));
								$main_menu = ob_get_clean();
								set_transient('primary_menu', $main_menu, 60*60*12);
								}
							echo $main_menu;
			        ?>
						</div>
						<div class="header__block header--mobile-trigger">
							<a href="#menu"><span></span></a>
						</div>
					</div>
					<div class="column fill-available">
						<div class="header__block header--search">
							<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
						</div>
						<div class="header__block header--misc" style="display: none;">
							<ul class="header-misc">
								<li><a href="#">Deals</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="headline">
	  <div class="container">
	    <div class="row row-wrap">
	      <div class="headline-content headline--arch">
	        <div class="column column-100">
	          <h1>WordPress <?php echo get_queried_object()->name; ?> <?php echo $post_fix ?></h1>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="nav">
	  <div class="container">
	    <div class="row row-wrap">
	      <div class="column column-100">
	        <div class="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
	            <?php
	            if(function_exists('bcn_display'))
	            {
	                    bcn_display();
	            }?>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>

	<div id="content" class="site-content">

<?php
/* Remove Shortlink
/***********************************************************************/
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action ('template_redirect', 'wp_shortlink_header', 11, 0);

/* Disable XML-RPC
/***********************************************************************/
add_filter('xmlrpc_enabled', '__return_false');
add_filter('wp_headers', 'perfclass_remove_x_pingback');
add_filter('pings_open', '__return_false', 9999);

function perfclass_remove_x_pingback($headers) {
    unset($headers['X-Pingback'], $headers['x-pingback']);
    return $headers;
}

/* Disable Self Pingbacks
/***********************************************************************/
add_action('pre_ping', 'perfclass_disable_self_pingbacks');

function perfclass_disable_self_pingbacks(&$links) {
	$home = get_option('home');
	foreach($links as $l => $link) {
		if(strpos($link, $home) === 0) {
			unset($links[$l]);
		}
	}
}


/* Remove REST API Links
/***********************************************************************/
remove_action('xmlrpc_rsd_apis', 'rest_output_rsd');
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('template_redirect', 'rest_output_link_header', 11, 0);


/* Disable Dashicons
/***********************************************************************/
// Un-comment the action below to disable Dashicons

//add_action('wp_enqueue_scripts', 'perfclass_disable_dashicons');

function perfclass_disable_dashicons() {
	if(!is_user_logged_in()) {
		wp_dequeue_style('dashicons');
	  wp_deregister_style('dashicons');
	}
}

/* Disable Emojis
/***********************************************************************/
function perfclass_disable_emojis() {
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	add_filter('tiny_mce_plugins', 'perfclass_disable_emojis_tinymce');
	add_filter('wp_resource_hints', 'perfclass_disable_emojis_dns_prefetch', 10, 2);
	add_filter('emoji_svg_url', '__return_false');
}

function perfclass_disable_emojis_tinymce($plugins) {
	if(is_array($plugins)) {
		return array_diff($plugins, array('wpemoji'));
	} else {
		return array();
	}
}

function perfclass_disable_emojis_dns_prefetch( $urls, $relation_type ) {
	if('dns-prefetch' == $relation_type) {
		$emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2.2.1/svg/');
		$urls = array_diff($urls, array($emoji_svg_url));
	}
	return $urls;
}

add_action('init', 'perfclass_disable_emojis');


/* Disable Embeds
/***********************************************************************/
function perfclass_disable_embeds() {
	global $wp;
	$wp->public_query_vars = array_diff($wp->public_query_vars, array('embed',));
	remove_action( 'rest_api_init', 'wp_oembed_register_route' );
	add_filter( 'embed_oembed_discover', '__return_false' );
	remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	add_filter( 'tiny_mce_plugins', 'perfclass_disable_embeds_tiny_mce_plugin' );
	add_filter( 'rewrite_rules_array', 'perfclass_disable_embeds_rewrites' );
	remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );
}

function perfclass_disable_embeds_tiny_mce_plugin($plugins) {
	return array_diff($plugins, array('wpembed'));
}

function perfclass_disable_embeds_rewrites($rules) {
	foreach($rules as $rule => $rewrite) {
		if(false !== strpos($rewrite, 'embed=true')) {
			unset($rules[$rule]);
		}
	}
	return $rules;
}

add_action('init', 'perfclass_disable_embeds', 9999);

/* Remove Query Strings
/***********************************************************************/
function perfclass_remove_query_strings() {
	if(!is_admin()) {
		add_filter('script_loader_src', 'perfclass_remove_query_strings_split', 15);
		add_filter('style_loader_src', 'perfclass_remove_query_strings_split', 15);
	}
}

function perfclass_remove_query_strings_split($src){
	$output = preg_split("/(&ver|\?ver)/", $src);
	return $output[0];
}

//add_action('init', 'perfclass_remove_query_strings');

/* Remove jQuery Migrate
/***********************************************************************/
function perfclass_remove_jquery_migrate(&$scripts) {
  if(!is_admin()) {
    $scripts->remove('jquery');
    $scripts->add('jquery', false, array( 'jquery-core' ), '1.12.4');
  }
}
add_filter('wp_default_scripts', 'perfclass_remove_jquery_migrate');

/* Hide WordPress Version
/***********************************************************************/
function perfclass_hide_wp_version() {
	return '';
}

remove_action('wp_head', 'wp_generator');
add_filter('the_generator', 'perfclass_hide_wp_version');


?>

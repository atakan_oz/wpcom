<?php
$home_support_args = array(
 'post_type' => 'resource',
 'tax_query' => array( array(
   'taxonomy' => 'resource_category',
   'field' => 'slug',
   'terms' => array('support'),
   'include_children' => false,
 )),
 'posts_per_page' => 5,
 'no_found_rows' => true,
 'meta_key' => 'resource_order',
 'orderby'=> 'meta_value_num',
 'order' => 'DESC',
 'post_status' => 'published',
);
?>

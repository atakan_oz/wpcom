<?php
/**
 * 	Featured Loop
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */
global $s3_preview_bucket;
global $cf_preview;

// Get Pod Fields
$pod = pods( 'resource', get_the_id());

// Retrieve Title
$res_title = get_the_title(get_the_id());
$res_image_title = strtolower(str_replace(' ', '', get_the_title(get_the_id())));

// Retrieve Slug
$res_slug = get_post_field( 'post_name', get_the_id());

// Define Featured Title for Screenshot
$featured_title = $res_slug . "-ss";

// Generate Screenshots
$featured_image_class = "screenshot";

// Define Icon Title for Screenshot
$icon_title = $res_slug . "-icon";

if (has_post_thumbnail(get_the_id())) {
  $featured_image = get_the_post_thumbnail_url();
  $featured_image_class = "thumbnail";
} elseif ($pod->display('icon') === 'Yes'){
  $featured_image = "https://d27tr3630ifmlb.cloudfront.net/icons/" . $icon_title . ".png";
}
elseif ($pod->display('website_preview')) {
  //$featured_image = "https://api.urlbox.io/v1/htoaerz4orfgkZxP/jpg?use_s3=true&s3_bucket=testscreenshot&s3_path=%2Fscreenshots%2F". $featured_title ."&url=" . $pod->display('screenshot_1') . "&thumb_width=300&quality=50&ttl=2592000";
  $featured_image = "https://d27tr3630ifmlb.cloudfront.net/screenshots/" . $featured_title . ".jpg";
}
elseif($pod->display('theme_preview')) {
  $featured_image = $cf_preview . $res_image_title . ".jpg";
}
else {
  //$featured_image = "https://api.urlbox.io/v1/htoaerz4orfgkZxP/jpg?use_s3=true&s3_bucket=testscreenshot&s3_path=%2Fscreenshots%2F". $featured_title ."&url=" . $pod->display('website_link') . "&thumb_width=300&quality=50&ttl=2592000";
  $featured_image = "https://d27tr3630ifmlb.cloudfront.net/screenshots/" . $featured_title . ".jpg";
}

// Retrieve Categories
$categories = get_the_terms(get_the_id(), 'resource_category' );
$first_category = (!empty( $categories[0])) ? true : true;
$second_category = (!empty( $categories[1])) ? true : true;

// Retrieve Selected & Selected Class
$selected = $pod->display('post_selected', get_the_id());
if($selected === "Yes") {
  $selected_class = 'selected-resource';
} else {
    $selected_class = '';
}

$featured = $pod->display('post_featured', get_the_id());
if($featured === "Yes") {
  $featured_class = 'featured-resource';
} else {
  $featured_class = '';
}

?>
<div id="resource-<?php echo get_the_id() ?>" class="resource-card <?php echo $featured_class ?>">
  <?php if(function_exists('wp_ulike')) wp_ulike('get'); ?>
  <a class="resource-link" href="<?php the_permalink(); ?>">
    <div class="column rsc-img lazy <?php echo $featured_image_class ?>" data-bg="url(<?php echo $featured_image ?>)"></div>
    <div class="column rsc-content">
      <span class="rsc-title <?php echo $selected_class ?>"><?php the_title(); ?></span>
      <span class="rsc-desc"><?php echo wp_trim_words(get_the_content(), 35, '...' ); ?></span>
    </div>
  </a>
  <div class="rsc-info">
    <div class="rsc-cat">
      <?php
      if($categories) {
        foreach($categories as $category) {
          echo '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '">' . esc_html( $category->name ) . '</a>';
        }
      }
      ?>
    </div>
    <div class="rsc-share">
      <?php get_template_part("templates/components/share-resource"); ?>
    </div>
  </div>
</div>

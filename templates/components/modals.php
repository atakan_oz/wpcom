<div id="modal--add-resource" class="modal modal--slide" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
			<button class="modal__close" data-micromodal-close></button>
			<header class="modal__header">
				<h2 class="modal__title" id="modal-1-title">Add Resource</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<?php echo do_shortcode('[contact-form-7 id="241" title="Add Resource"]'); ?>
			</main>
		</div>
	</div>
</div>

<div id="modal--report" class="modal modal--slide" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
			<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
			<header class="modal__header">
				<h2 class="modal__title" id="modal-1-title">Report Resource</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<?php echo do_shortcode('[contact-form-7 id="412" title="Report Resource"]'); ?>
			</main>
		</div>
	</div>
</div>


<div id="modal--startup" class="modal modal--slide" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
			<header class="modal__header">
				<img src="<?php echo get_template_directory_uri() ?>/dist/img/startupcom-logo.svg" alt="wpcom-logo">
				<h2 class="modal__title" id="modal-1-title">StartupCOM is almost ready...</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<p>StartupCom will be the biggest curated list of Startup tools where entrepreneurs will benefit from it, just like the predecessors WPCOM.</p>
				<p>We're already prepared for more than 300 resources for you. Join our waiting list and be the first one to discover them.</p>
			</main>
		</div>
	</div>
</div>

<div id="modal--marketing" class="modal modal--slide" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
			<header class="modal__header">
				<img src="<?php echo get_template_directory_uri() ?>/dist/img/marketingcom-logo.svg" alt="wpcom-logo">
				<h2 class="modal__title" id="modal-1-title">MarketingCOM is almost ready...</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<p>MarketingCom will be the biggest curated list of Marketing tools where entrepreneurs will benefit from it, just like the predecessors WPCOM.</p>
				<p>We're already prepared for more than 270 resources for you. Join our waiting list and be the first one to discover them.</p>
			</main>
		</div>
	</div>
</div>

<div id="modal--seo" class="modal modal--slide" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
			<header class="modal__header">
				<img src="<?php echo get_template_directory_uri() ?>/dist/img/seocom-logo.svg" alt="wpcom-logo">
				<h2 class="modal__title" id="modal-1-title">SEOCOM is almost ready...</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<p>SEOCom will be the biggest curated list of SEO tools where entrepreneurs will benefit from it, just like the predecessors WPCOM.</p>
				<p>We're already prepared for more than 300 resources for you. Join our waiting list and be the first one to discover them.</p>
			</main>
		</div>
	</div>
</div>

<div id="modal--share" class="modal modal--slide modal--colored" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
			<header class="modal__header">
				<img src="<?php echo get_template_directory_uri() ?>/dist/img/email.svg" alt="">
				<h2 class="modal__title" id="modal-1-title">Find the one? :)</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<p>You may not have enough time to explore 300+ resources but you can always let us send you a curated list of new and popular resources and <i>special WordPress deals</i> to your inbox.</p>
				<form class="subscribe__form">
					<input id="mc-email" type="email" name="EMAIL" placeholder="Email address...">
					<button type="submit">Submit</button>
				</form>
				<span class="ps">Feeds of both news and popular resources will be sent to your inbox, once a month.</span>
			</main>
		</div>
	</div>
</div>

<div id="modal--share" class="modal modal--slide modal--colored" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
			<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
			<header class="modal__header">
				<img src="<?php echo get_template_directory_uri() ?>/dist/img/email.svg" alt="">
				<h2 class="modal__title" id="modal-1-title">Leaving already?</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<p>Okay, you may not have enough time to explore 300+ resources but let us send you curated list of both news and popular resources to your inbox.</p>
				<form class="subscribe__form">
					<input id="mc-email" type="email" name="EMAIL" placeholder="Email address...">
					<button type="submit">Submit</button>
				</form>
				<span class="ps">Feeds of both news and popular resources will be sent to your inbox, once a month.</span>
			</main>
		</div>
	</div>
</div>

<div id="modal--reply" class="modal modal--slide modal--colored" aria-hidden="true">
	<div class="modal__overlay" tabindex="-1" data-micromodal-close>
		<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
			<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
			<header class="modal__header">
				<h2 class="modal__title" id="modal-1-title">Your comments matters.</h2>
			</header>
			<main class="modal__content" id="modal-1-content">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc gravida eget sapien vel rutrum. Quisque cursus nec est a fringilla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>
				<hr>
				<div class="social__block">
					<span>Share On: </span>
					<?php get_template_part("templates/components/share-website"); ?>
				</div>
			</main>
		</div>
	</div>
</div>

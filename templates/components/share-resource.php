<a class="facebook" rel="nofollow" target="_blank" href="https://www.facebook.com/sharer.php?u=<?php echo the_permalink(); ?>"><i class="icon icon_facebook"></i> </a>
<a class="twitter" rel="nofollow" target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo the_permalink(); ?>"><i class="icon icon_twitter"></i> </a>
<a class="linkedin" rel="nofollow" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo the_permalink(); ?>"><i class="icon icon_linkedin"></i> </a>
<a class="thumblr" rel="nofollow" target="_blank" href="https://www.tumblr.com/widgets/share/tool?canonicalUrl=<?php echo the_permalink(); ?>"><i class="icon icon_tumblr"></i> </a>
<a class="pinterest" rel="nofollow" target="_blank" href="http://pinterest.com/pin/create/link/?url=<?php echo the_permalink(); ?>"><i class="icon icon_pinterest"></i> </a>
<a class="mail" rel="nofollow" target="_blank" href="mailto:<?php echo the_permalink(); ?>"><i class="icon icon_mail"></i> </a>

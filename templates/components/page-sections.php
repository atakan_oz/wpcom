<div id="subscribe-box" class="column column-100">
  <div class="row row-wrap">
    <!-- Subscribe Starts  -->
    <div id="subscribe" class="column column-100">
      <div class="container">
        <div class="subscribe-box lazy">
          <div class="subscribe-content">
            <h5>Get Latest Resources </h5>
            <p>You may not have enough time to explore 300+ resources but you can always let us send you a curated list of both news and popular resources to your inbox.</p>
            <form class="subscribe__form">
              <input id="mc-email" type="email" name="EMAIL" placeholder="Email address...">
              <button type="submit">Submit</button>
              <span class="form-notification success">You are successfully subscribed!</span>
              <span class="form-notification failure">Either you are already subscribed or we messed up.</span>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Subscribe Ends  -->
  </div>
</div>

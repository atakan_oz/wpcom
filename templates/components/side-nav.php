<div id="sidebar" class="column column-25" data-sticky-container>
  <div class="row row-wrap" style="justify-content: space-between;">
    <div id="categories" class="widget widget--categories">
      <div class="section-title"><span class="cat"><i class="icon icon_cat"></i></span> Navigator</div>
        <div class="rsc-categories">
          <?php if(is_tax('resource_category')): ?>
          <?php
          // Variables
          $main_cat_id = get_queried_object();
          $main_cat_name = $main_cat_id->name;
          $secondary_cat_id = $main_cat_id->parent;
          $website_link = esc_url( home_url( '/' ) );
          $main_cat_url = get_category_link($main_cat_id->id);
          $secondary_cat_url = get_category_link($secondary_cat_id);

          // Main Category
          echo '<div class="main-navigator">';
          echo '<h5 href="'. $main_cat_url .'">Best WordPress '. $main_cat_name .'</h5><p>There are <i>'. $main_cat_id->count .' tools</i>, plugins or services in this particular category.</p>';
          // Parent Category
          echo '</div>';
          ?>
          <div class="cat-navigator">
            <h5>Sub-Categories</h5>
            <?php
              // Sub Categories
              $show_count = 1; // 1 for yes, 0 for no
              $hierarchical = 1; // 1 for yes, 0 for no
              $taxonomy = 'resource_category';
              $title = '';
              $args = array(
                'parent'        =>  get_queried_object_id(),
                'orderby' => $orderby,
                'show_count' => $show_count,
                'hierarchical' => $hierarchical,
                'taxonomy' => $taxonomy,
                'title_li' => $title,
                'use_desc_for_title' => 0,
                'echo' => 0,
                'show_option_none' => 'There are no subcategories'
              );
              echo '<ul>';
                $categories = wp_list_categories($args);
                $categories = preg_replace('/<\/a> \(([0-9]+)\)/', ' <span class="count">\\1</span></a>', $categories);
                echo $categories;
              echo '</ul>'
            ?>
          </div>
        <?php else: ?>
          <div class="cat-navigator">
            <h5>Categories</h5>
            <?php
              // Sub Categories
              $show_count = 1; // 1 for yes, 0 for no
              $hierarchical = 1; // 1 for yes, 0 for no
              $taxonomy = 'resource_category';
              $title = '';
              $args = array(
                'depth' => 1,
                'orderby' => '',
                'show_count' => $show_count,
                'hierarchical' => $hierarchical,
                'taxonomy' => $taxonomy,
                'title_li' => $title,
                'use_desc_for_title' => 0,
                'echo' => 0,
                'show_option_none' => 'There are no subcategories'
              );
              echo '<ul>';
                $categories = wp_list_categories($args);
                $categories = preg_replace('/<\/a> \(([0-9]+)\)/', ' <span class="count">\\1</span></a>', $categories);
                echo $categories;
              echo '</ul>'
            ?>
          </div>
        <?php endif; ?>
        <div class="tag-navigator">
          <h5>Popular Tags</h5>
          <ul>
            <?php
              $tags = get_terms('resource_tag', array(
               'orderby' => 'rand',
               'number'  => 7,
              ));
              foreach ((array) $tags as $tag ) {
                 echo '<li><a href="' . get_tag_link ($tag->term_id) . '" rel="tag">#' . $tag->name . '<span> '. $tag->count .'</span></a></li>';
              }
            ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="widget widget--ss2">
      <div class="section-title"><span class="recommend"><i class="icon icon_check-circle"></i></span> We recommend</div>
      <div class="ss2-box">
        <a target="_blank" href="https://codeable.io/?ref=g3TGR"><img class="lazy" src="" data-src="https://referoo.co/creatives/8/asset.png" /></a>
      </div>
    </div>
  </div>
</div>

<div id="sidebar" class="column column-25" data-sticky-container>
  <div class="row row-wrap" style="justify-content: space-between;">
    <div id="categories" class="widget widget--categories">
      <div class="section-title"><span class="cat"><i class="icon icon_cat"></i></span> Subscribe</div>
      <div class="subscribe-content">
        <h5>Get the best resources</h5>
        <p>Subscribe to us and be aware of more resources like Kinsta.</p>
      </div>
    </div>
    <div class="widget widget--ss2">
      <div class="section-title"><span class="recommend"><i class="icon icon_check-circle"></i></span> We recommend</div>
      <div class="ss2-box">
        <a target="_blank" href="https://codeable.io/?ref=g3TGR"><img class="lazy" src="" data-src="https://referoo.co/creatives/8/asset.png" /></a>
      </div>
    </div>
  </div>
</div>

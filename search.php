<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package wpcom
 */

get_header();

global $wp_query;
$search_query = get_search_query();
?>

<div class="headline custom-page">
  <div class="headline-overlay"></div>
  <div class="container">
    <div class="row row-wrap">
      <div class="headline-content headline--search">
        <div class="column column-100">
          <h1>Search results for: <?php echo get_search_query(); ?></h1>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="content">
  <div class="container">
    <div class="row row-wrap">
      <!-- Sidebar Starts -->
      <?php get_template_part( 'templates/components/side-nav'); ?>
      <!-- Sidebar Ends -->
      <!-- Home Sections Starts -->
      <div id="archive" class="column column-75">
        <div class="row row-wrap">
          <div class="column column--section column-featured column-100">
            <div class="row row-wrap">
              <div class="column column-100">
                <div class="resource-loop">
                  <?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="6" offset="0"  pause="true" offset="0" posts_per_page="6" nested="true" post_type="resource" search="'. $search_query .'" orderby="relevance" taxonomy_operator="IN" scroll="false" button_label="More Resources" button_loading_label="Loading Resources..."]'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Home Sections Ends -->
  <?php get_template_part( 'templates/components/page-sections'); ?>
</div>

<?php get_footer(); ?>

<?php
/**
 * 	Archive Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */


global $wp_query;
$cat = get_queried_object();
$term = $cat->term_id;
?>
<?php get_header(); ?>

<div class="headline custom-page">
  <div class="headline-overlay"></div>
  <div class="container">
    <div class="row row-wrap">
      <div class="headline-content headline--404">
        <div class="column column-100">
          <h1><?php esc_html_e( 'Oops! That archive can&rsquo;t be found.', 'wpcom' ); ?></h1>
          <p><?php esc_html_e( 'It looks like you are searching in the wrong category base. Try /wordpress/category or use the search box below.', 'wpcom' ); ?></p>
          <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wpcom
 */

?>

	</div><!-- #content -->
	<div id="menu">
		<?php
			wp_nav_menu(array(
				'theme_location' =>  'header-nav',
				'container'      =>  false,
				'items_wrap'     =>  my_nav_wrap(),
				'walker'         =>  new wpcom_main_nav(),
				'depth'          =>  '2',
			));
		?>
	</div>
	<footer>
		<div class="container">
			<div class="row row-wrap">
				<div class="footer-block column column-35 footer--copyright">
					<a class="logo-dark" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri() ?>/dist/img/wpcom-logo-dark.svg" alt="wpcom-dark-logo"></a>
					<p>WPCOM is a community/directory platform that gathered more than 350 WordPress related resources to save time when you're building your WordPress website.</p>
					<div class="com-social">
						<a href="https://www.facebook.com/wpcomorg/" target="_blank"><i class="icon icon_facebook"></i></a>
						<a href="https://twitter.com/wpcomorg" target="_blank"><i class="icon icon_twitter"></i></a>
					</div>
				</div>
				<div class="footer-block column column-15 footer--nav">
					<span class="footer-nav-title">Navigation</span>
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-tools">Tools</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-hosting">Hosting</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-themes">Themes</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-support">Support</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>resources/wordpress-courses">Courses</a> </li>
					</ul>
				</div>
				<div class="footer-block column column-15 footer--nav">
					<span class="footer-nav-title">Misc</span>
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>about">About</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>advertising">Advertising</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>cookie-policy">Cookie Policy</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>privacy-policy">Privacy Policy</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact">Contact</a> </li>
					</ul>
				</div>
				<div class="footer-block column column-15 footer--nav">
					<span class="footer-nav-title">Actions</span>
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>add-resource">Add Resource</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>report-resource">Report Resource</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact">Suggestions</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact">Partnership</a> </li>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>hire-experts">Hire Experts</a> </li>
					</ul>
				</div>
				<div class="footer-block column column-20 footer--nav">
					<span class="footer-nav-title">Tags</span>
					<ul>
						<li><a href="#">#Best WordPress Deals</a> </li>
						<li><a href="#">#Fast WordPress Hosting</a> </li>
						<li><a href="#">#Free WordPress Themes</a> </li>
						<li><a href="#">#Fast WordPress Themes</a> </li>
						<li><a href="#">#Hire WordPress Developers</a> </li>
					</ul>
				</div>
				<div class="footer-block column column-100 footer-nav footer-copy">
					<span>WPCOM @<?php echo date("Y"); ?>. All rights reserved.</span>
				</div>
			</div>
		</div>
	</footer
</div><!-- #page -->

<div class="mobile-navigator">
	<a href="#menu"><span class="icon icon_menu"></span> Categories</a>
	<a href="#">Go to Top</a>
</div>

<script>
  var OneSignal = window.OneSignal || [];
  OneSignal.push(function() {
    OneSignal.init({
      appId: "a434a559-fed3-4115-b411-3cad5f7ef25c",
    });
  });
</script>

<script>
  (function(a,b,c){var d=a.history,e=document,f=navigator||{},g=localStorage,
  h=encodeURIComponent,i=d.pushState,k=function(){return Math.random().toString(36)},
  l=function(){return g.cid||(g.cid=k()),g.cid},m=function(r){var s=[];for(var t in r)
  r.hasOwnProperty(t)&&void 0!==r[t]&&s.push(h(t)+"="+h(r[t]));return s.join("&")},
  n=function(r,s,t,u,v,w,x){var z="https://www.google-analytics.com/collect",
  A=m({v:"1",ds:"web",aip:c.anonymizeIp?1:void 0,tid:b,cid:l(),t:r||"pageview",
  sd:c.colorDepth&&screen.colorDepth?screen.colorDepth+"-bits":void 0,dr:e.referrer||
  void 0,dt:e.title,dl:e.location.origin+e.location.pathname+e.location.search,ul:c.language?
  (f.language||"").toLowerCase():void 0,de:c.characterSet?e.characterSet:void 0,
  sr:c.screenSize?(a.screen||{}).width+"x"+(a.screen||{}).height:void 0,vp:c.screenSize&&
  a.visualViewport?(a.visualViewport||{}).width+"x"+(a.visualViewport||{}).height:void 0,
  ec:s||void 0,ea:t||void 0,el:u||void 0,ev:v||void 0,exd:w||void 0,exf:"undefined"!=typeof x&&
  !1==!!x?0:void 0});if(f.sendBeacon)f.sendBeacon(z,A);else{var y=new XMLHttpRequest;
  y.open("POST",z,!0),y.send(A)}};d.pushState=function(r){return"function"==typeof d.onpushstate&&
  d.onpushstate({state:r}),setTimeout(n,c.delay||10),i.apply(d,arguments)},n(),
  a.ma={trackEvent:function o(r,s,t,u){return n("event",r,s,t,u)},
  trackException:function q(r,s){return n("exception",null,null,null,null,r,s)}}})
  (window,"UA-150756231-1",{anonymizeIp:true,colorDepth:true,characterSet:true,screenSize:true,language:true});
</script>

		<?php if(is_singular()):?>
<script>
OneSignal.push(function() {
  OneSignal.showNativePrompt();
	OneSignal.showSlidedownPrompt();
});
</script>
<?php endif;?>

<?php get_template_part("templates/components/modals"); ?>

<?php wp_footer(); ?>

</body>
</html>

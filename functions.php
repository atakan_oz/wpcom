<?php
/**
 * wpcom functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package wpcom
 */


// Global Variables

$s3_preview_bucket = "https://s3-us-west-1.amazonaws.com/wpcom.org/previews/";
$cf_preview = "https://d27tr3630ifmlb.cloudfront.net/previews/";

if ( ! function_exists( 'wpcom_setup' ) ) :
	// Sets up theme defaults and registers support for various WordPress features.
	function wpcom_setup() {
		// Make theme available for translation.
		load_theme_textdomain( 'wpcom', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		// Let WordPress manage the document title.
		add_theme_support( 'title-tag' );

		// Enable support for Post Thumbnails on posts and pages.
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'wpcom' ),
		) );
		show_admin_bar(false);
		// Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'wpcom_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for core custom logo.
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'wpcom_setup' );

// Register widget area.
function wpcom_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'wpcom' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'wpcom' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wpcom_widgets_init' );

// Enqueue scripts and styles.
function wpcom_scripts() {
	wp_enqueue_style( 'wpcom-style', get_stylesheet_uri() );
    wp_enqueue_style( 'wpcom-app-style', get_template_directory_uri() . '/dist/css/app.min.css' );
    wp_enqueue_script( 'wpcom-app-script', get_template_directory_uri() . '/dist/js/app.js' );
		wp_enqueue_script( 'wpcom-onesignal-script', get_template_directory_uri() . '/dist/js/onesignal.js' );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wpcom_scripts' );

// Register Navigation Menus
function custom_navigation_menus() {

	$locations = array(
		'header-nav' => __( 'Header Navigation', 'wpcom' ),
		'header-misc' => __( 'Header Misc', 'wpcom' ),
		'footer-nav' => __( 'Footer Navigation', 'wpcom' ),
	);
	register_nav_menus( $locations );

}
add_action( 'init', 'custom_navigation_menus' );

// Include Walker Template
require get_template_directory() . '/templates/walkers/main-nav.php';

// Include PerfClass
require get_template_directory() . '/templates/perfclass.php';

// Built Popular Tags
function wpb_tag_cloud() {
	$tags = get_terms('resource_tag' );
	$args = array(
	    'smallest'                  => 15,
	    'largest'                   => 15,
	    'unit'                      => 'px',
	    'number'                    => 10,
	    'format'                    => 'flat',
	    'separator'                 => " ",
	    'orderby'                   => 'count',
	    'order'                     => 'DESC',
	    'show_count'                => 1,
	    'echo'                      => true
	);
	$tag_string = wp_generate_tag_cloud( $tags, $args );
	return '<div class="tags_cloud">'.$tag_string.'</div>';
}

// Add a shortcode so that we can use it in widgets, posts, and pages
add_shortcode('wpb_popular_tags', 'wpb_tag_cloud');

// Enable shortcode execution in text widget
add_filter ('widget_text', 'do_shortcode');

// Pre Get Posts
function myplugin_pre_get_posts( $query ) {
	if (is_tax('resource_category') && $query->is_main_query() ) {
     // set your improved arguments
 		$query->set('posts_per_page', 15);
    $query->set('ignore_sticky_posts', true);
 		$query->set('post__not_in', get_option('sticky_posts'));
 		//$query->set('no_found_rows', true);
		$query->set('post_status', 'publish');
		$query->set('meta_key', 'selected_resource');
		$query->set('orderby', 'meta_value_num');
		$query->set('order', 'DESC');
  }
  if (is_tax('resource_tag') && $query->is_main_query() ) {
     // set your improved arguments
 		$query->set('posts_per_page', 8);
    $query->set('ignore_sticky_posts', true);
 		$query->set('post__not_in', get_option('sticky_posts'));
  }
}
add_action( 'pre_get_posts', 'myplugin_pre_get_posts', 99 );

// Related Posts
function related_posts( $post_id, $related_count, $args = array() ) {
	$terms = get_the_terms( $post_id, 'resource_category' );
	if ( empty( $terms ) ) $terms = array();
	$term_list = wp_list_pluck( $terms, 'slug' );
	$related_args = array(
		'post_type' => 'resource',
		'posts_per_page' => 5,
		'post_status' => 'publish',
		'post__not_in' => array( $post_id ),
		'orderby' => 'rand',
		'no_found_rows' => true,
	  'cache_results' => false,
	  'update_post_term_cache' => false,
	  'update_post_meta_cache' => false,
		'tax_query' => array(
			array(
				'taxonomy' => 'resource_category',
				'field' => 'slug',
				'terms' => $term_list
			)
		)
	);
	return new WP_Query( $related_args );
}

// END Custom Scripting to Move JavaScript

add_filter('wpcf7_autop_or_not', '__return_false');

function convert_ranktmath_title($post_id) {
  $title = RankMath\Post::get_meta( 'title', $post_id );
  return $title;
}

add_filter('wp_ulike_format_number','wp_ulike_new_format_number',10,3);
function wp_ulike_new_format_number($value, $num, $plus){
	if ($num >= 1000 && get_option('wp_ulike_format_number') == '1'):
	$value = round($num/1000, 2) . 'K';
	else:
	$value = $num;
	endif;
	return $value;
}

add_filter( "rank_math/opengraph/facebook/image", function( $attachment_url ) {
	if(is_singular()){
		global $post;
	  $post_slug = $post->post_name;
		$attachment_url = 'https://d27tr3630ifmlb.cloudfront.net/screenshots/' . $post->post_name . '-ss.jpg';
	}
	return $attachment_url;
});
add_filter( "rank_math/opengraph/twitter/image", function( $attachment_url ) {
	if(is_singular()){
		global $post;
	  $post_slug = $post->post_name;
		$attachment_url = 'https://d27tr3630ifmlb.cloudfront.net/screenshots/' . $post->post_name . '-ss.jpg';
	}
	return $attachment_url;
});

add_filter( 'rank_math/sitemap/enable_caching', '__return_false');

add_action( 'rank_math/head', function() {
	global $wp_filter;
	if ( isset( $wp_filter["rank_math/json_ld"] ) ) {
		unset( $wp_filter["rank_math/json_ld"] );
	}
});

function register_shortcodes() {
    add_shortcode( 'comitem', 'shortcode_comitem' );
}
add_action( 'init', 'register_shortcodes' );

function shortcode_comitem( $atts ) {
	global $wp_query,
	$post;

	$atts = shortcode_atts( array(
	    'rid' => ''
	), $atts );

	$loop = new WP_Query( array(
    'posts_per_page'    => 1,
    'post_type'         => 'resource',
		'p' => $atts['rid']
	));
	if( ! $loop->have_posts() ) {
        return false;
    }

    while( $loop->have_posts() ) {
        $loop->the_post();
				get_template_part('/templates/posts/standard');
    }

    wp_reset_postdata();
}

function my_nav_wrap() {
  // default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'
	$web = $_SERVER['SERVER_NAME'];
  // open the <ul>, set 'menu_class' and 'menu_id' values
  $wrap  = '<ul class="header-menu2">';

	$wrap .= '<li class="mobile-search menu-item menu-item-type-taxonomy menu-item-object-resource_category"><div class="mmenu-search"><input type="text" placeholder="Search..." value=""></div></li>';

	$wrap .= '<div class="section lvl--1"><span class="nav-separator">Categories</span>';
  // get nav items as configured in /wp-admin/
  $wrap .= '%3$s</div>';
	$wrap .= '<div class="section lvl--2"><span class="nav-separator">Misc</span>';
  // the static link
  $wrap .= '<li class="my-static-link"><a href="'. $web .'/add-resource">Add Resource</a></li>';
	$wrap .= '<li class="my-static-link"><a href="'. $web .'/report-resource">Report Resource</a></li>';
	$wrap .= '<li class="my-static-link"><a href="'. $web .'/about">About</a></li>';

  // close the <ul>
  $wrap .= '</ul>';
  // return the result
  return $wrap;
}


?>

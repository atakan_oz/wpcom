<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wpcom
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<?php if ( ! function_exists( '_wp_render_title_tag' ) ) :
    function theme_slug_render_title() {
  ?>
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php
    }
    add_action( 'wp_head', 'theme_slug_render_title' );
	endif;
	?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<script data-ad-client="ca-pub-4943041254401527" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

<div class="global__nav">
	<div class="container">
		<div class="row row-wrap row-no-padding">
			<div class="column column-100 header-master">
				<div class="column">
					<div class="header__block header--sites">
						<ul>
							<li class="wpcom active"><a href="https://www.wpcom.org">WordPress</a></li>
							<li class="startupcom"><a data-micromodal-trigger="modal--startup" href="#">Startups</a></li>
							<li><a href="#" data-micromodal-trigger="modal--marketing">Marketing</a></li>
							<li><a href="#" data-micromodal-trigger="modal--seo">SEO</a></li>
						</ul>
					</div>
					<div class="header__block header--brand">
						<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri() ?>/dist/img/wpcom-logo-mobile.svg" alt="wpcom-logo"></a>
					</div>
					<div class="header__block header--account">
						<ul>
							<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>report-resource">Report</a></li>
							<li class="submit"><a href="<?php echo esc_url( home_url( '/' ) ); ?>add-resource">Submit Resource</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="head">
	<header class="main__header">
		<div class="container">
			<div class="row row-wrap row-no-padding">
				<div class="column column-100 header-master">
					<div class="column">
						<div class="header__block header--menu">
							<a class="mobile-trigger" href="#menu"><i class="icon icon_menu"></i> Navigation</a>
							<?php
							if(false === ($main_menu = get_transient('primary_menu'))) {
								ob_start();
		            wp_nav_menu(array(
		              'theme_location' =>  'header-nav',
		              'container'      =>  false,
		              'items_wrap'     =>  '<ul class="header-menu">%3$s</ul>',
		              'walker'         =>  new wpcom_main_nav(),
		              'depth'          =>  '2',
		            ));
								$main_menu = ob_get_clean();
								set_transient('primary_menu', $main_menu, 60*60*12);
								}
							echo $main_menu;
			        ?>
						</div>
						<div class="header__block header--mobile-trigger">
							<a href="#menu"><span></span></a>
						</div>
					</div>
					<div class="column fill-available">
						<div class="header__block header--search">
							<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
						</div>
						<div class="header__block header--misc" style="display: none;">
							<ul class="header-misc">
								<li><a href="#">Deals</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="headline">
	  <div class="container">
	    <div class="row row-wrap">
	      <div class="headline-content">
	        <div class="column column-100">
	          <h1>All about WordPress — in one place.</h1>
						<h2>Explore hundreds of WordPress resources, courses, deals. <br> Build your website with confidence and less pain.</h2>
						<button type="button" name="button"><a href="<?php echo esc_url( home_url( '/' ) ); ?>most-recent">Discover Most Recent</a></button>
						<button type="button" name="button"><a href="<?php echo esc_url( home_url( '/' ) ); ?>most-popular">Discover Most Popular</a></button>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="headline-featured">
		<div class="container">
			<div class="row row-wrap">
				<div class="column column-100">
					<span class="featured-title">As Seen On</span>
					<div class="featured-images">
						<a href="https://www.producthunt.com/posts/wpcom" target="_blank"><img class="producthunt" src="<?php echo get_template_directory_uri() ?>/dist/img/producthunt.png" alt=""></a>
						<a href="#" target="_blank"><img class="knowtechie" src="<?php echo get_template_directory_uri() ?>/dist/img/knowtechie.png" alt=""></a>
						<a href="https://betalist.com/startups/wpcom" target="_blank"><img class="betalist" src="<?php echo get_template_directory_uri() ?>/dist/img/betalist.png" alt=""></a>
						<a href="https://news.ycombinator.com/item?id=21523494" target="_blank"><img class="hackernews" src="<?php echo get_template_directory_uri() ?>/dist/img/hackernews.png" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<div id="content" class="site-content">

//------------------------------------------
/**
* WPCOM App Script
* @version 1.0.0 (Initial Version)
* @author Atakan Oz
* @copyright @2019 ATAKANOZ
* @todo Optimize further!
*/

// Define $ as Jquery
$ = jQuery;

$(function() {
  var wpcomScript = {
    domManipulate: function(){

      $(".comment-form-author, .comment-form-email, .comment-form-url, .comment-form-cookies-consent").hide();
      $(".comment-form-comment").click(function(event) {
        $(".comment-form-author, .comment-form-email, .comment-form-url, .comment-form-cookies-consent").show();
      });
      $(".rsc-categories #nav-menu-item-7 .main-menu-link").append("<span class='badge badge--hot'>HOT</span>");
      $(".header-more").click(function(event) {
        $(this).toggleClass("open");
        $(".main-nav").removeClass("open");
        $('.nav-trigger .icon').removeClass('icon_close');
        $('.nav-trigger .icon').addClass('icon_menu');
      });
      $(".nav-trigger").click(function(event) {
        $(".main-nav").toggleClass("open");
        $(".header-more").removeClass("open");
      });
      $.fn.toggle2classes = function(class1, class2){
        if( !class1 || !class2 )
          return this;
        return this.each(function(){
          var $elm = $(this);
          if( $elm.hasClass(class1) || $elm.hasClass(class2) )
            $elm.toggleClass(class1 +' '+ class2);
          else
            $elm.addClass(class1);
        });
      };
      $(".nav-trigger").click(function(event) {
        $('.nav-trigger .icon').toggle2classes('icon_menu', 'icon_close'); //Adds 'a', removes 'b' and vice versa
      });
      $(".search-trigger").click(function(event) {
        $("header .header-master .column.fill-available").slideToggle();
      });
      $(".search-trigger").click(function(event) {
        $('.search-trigger .icon').toggle2classes('icon_search', 'icon_close'); //Adds 'a', removes 'b' and vice versa
      });
    },
    ajaxChimp: function() {
      $('.subscribe__form').ajaxChimp({
        url: 'https://wpcom.us20.list-manage.com/subscribe/post?u=bdfef8b57f2c8835236fcc43a&amp;id=df8c2747f0',
        callback: callbackFunction
      });
      function callbackFunction (resp) {
       if (resp.result === 'success') {
          $('.subscribe__form span.success').show();
          console.log('Success::Subscription');
       } else {
         $('.subscribe__form span.failure').show();
         console.log('Fail::Subscription');
       }
      }
    },
    lazyload: function(){
      var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
        // ... more custom settings?
      });
    },
    sticky: function(){

    },
    accordionMenu: function(){
      $.fn.extend({
        // Define the accordionMenu() function that adds the sliding functionality
        acMenu: function(){
          return this.each(function(){
            var menuItems = $(".main-nav").children('li');
            menuItems.find('.sub-menu').parent().addClass('has-child');
            //$('.main-nav .has-child ul').hide();
            $('.main-nav .has-child > a').on('click', function(event) {
              event.stopPropagation();
              event.preventDefault();
              $(this).siblings().toggleClass('open');
              $(this).toggleClass('is-toggled');
            });
          });
        }
      });
      $('.main-nav').acMenu();

      $.fn.extend({
        // Define the accordionMenu() function that adds the sliding functionality
        acSideMenu: function(){
          return this.each(function(){
            var menuItems = $(".cat-nav").children('li');
            menuItems.find('.sub-menu').parent().addClass('has-child');
            //$('.main-nav .has-child ul').hide();
            $('.cat-nav .has-child > a').on('click', function(event) {
              event.stopPropagation();
              event.preventDefault();
              $(this).siblings().toggleClass('open');
              $(this).toggleClass('is-toggled');
            });
          });
        }
      });
      $('.cat-nav').acSideMenu();
    },
    carousels: function() {
      // Release Owl Carousel for Single Screenshot
      $('.cat-carousel').owlCarousel({
        margin:25,
        loop:true,
        items:3,
        dots: false,
        nav: true,
        autoplay:false,
        autoplayTimeout: 6000,
        autoplayHoverPause:true,
        autoplaySpeed: 1000,
        lazyLoad: true,
        navText: ["<i class='icon icon_chevron-left'></i>","<i class='icon icon_chevron'></i>"],
        responsive:{
          0:{
              items:1,
              loop: false
          },
          599:{
              items:1,
              loop: false
          },
          600:{
              items:2,
          },
          1100:{
              items:3,
              loop: false,
          }
        }
      });
      $('.rsc-ss').owlCarousel({
        margin:25,
        loop:true,
        autoWidth:true,
        items:2,
        dots: true,
        autoplay:true,
        autoplayTimeout:4000,
        autoplayHoverPause:true,
        lazyLoad: true
      });
      $('.owl-featured').owlCarousel({
        margin:25,
        loop:true,
        items:4,
        dots: false,
        nav: true,
        autoplay:false,
        autoplayTimeout: 6000,
        autoplayHoverPause:true,
        autoplaySpeed: 1000,
        lazyLoad: true,
        navText: ["<i class='icon icon_chevron-left'></i>","<i class='icon icon_chevron'></i>"],
        responsive:{
          0:{
              items:1,
              loop: false
          },
          768:{
              items:2,
              loop: false
          },
          1024:{
              items:3,
          },
          1100:{
              items:4,
              loop: false,
          }
        }
      });
      $('.owl-featured-single').owlCarousel({
        margin:25,
        loop:true,
        items:1,
        dots: false,
        nav: true,
        autoplay:false,
        autoplayTimeout: 6000,
        autoplayHoverPause:true,
        autoplaySpeed: 1000,
        lazyLoad: true,
        navText: ["<i class='icon icon_chevron-left'></i>","<i class='icon icon_chevron'></i>"],
        responsive:{
          0:{
              items:1,
              loop: false
          },
          599:{
              items:1,
              loop: false
          },
          600:{
              items:1,
          },
          1100:{
              items:1,
              loop: false,
          }
        }
      });
    },
    init: function() {
      wpcomScript.ajaxChimp();
      wpcomScript.lazyload();
      wpcomScript.sticky();
      wpcomScript.accordionMenu();
      wpcomScript.carousels();
      ///
      wpcomScript.domManipulate();
    }
  }
  $(function() {
    // Init all the scripts above
    wpcomScript.init();
    MicroModal.init();
    console.log("%c Everything is well and working...", "background: #34bf49; color: #FFF");
  });
});

$(document).ready(function() {
  // Check if Knockout.JS is available
  // Knockout is using for front-end development stage at WPCOM
  $(window).scroll(function(){
    if ($(window).scrollTop() >= 400) {
      $('.header-filler').addClass('active');
      $('header').addClass('fixed');
    }
    else {
      $('.header-filler').removeClass('active');
      $('header').removeClass('fixed');
    }
  });
  $('input, textarea, select').each(function(index, el) {
    $(this).keypress(function() {
      if($(this).val().length === 0 ) {
        $(this).removeClass("has-content");
      } else {
        $(this).addClass("has-content");
      }
    });
    $(this).change(function() {
      if($(this).val().length === 0 ) {
        $(this).removeClass("has-content");
      } else {
        $(this).addClass("has-content");
      }
    });
  });
});

window.addEventListener("load", function() {
	// store tabs variable
	var myTabs = document.querySelectorAll("ul.nav-tabs > li");
  function myTabClicks(tabClickEvent) {
		for (var i = 0; i < myTabs.length; i++) {
			myTabs[i].classList.remove("active");
		}
		var clickedTab = tabClickEvent.currentTarget;
		clickedTab.classList.add("active");
		tabClickEvent.preventDefault();
		var myContentPanes = document.querySelectorAll(".tab-pane");
		for (i = 0; i < myContentPanes.length; i++) {
			myContentPanes[i].classList.remove("active");
		}
		var anchorReference = tabClickEvent.target;
		var activePaneId = anchorReference.getAttribute("href");
		var activePane = document.querySelector(activePaneId);
		activePane.classList.add("active");
	}
	for (i = 0; i < myTabs.length; i++) {
		myTabs[i].addEventListener("click", myTabClicks)
	}
});

$("#tabs li").each(function(index, el) {
  $(this).click(function(event) {
    $('.owl-featured').owlCarousel();
  });
});


document.addEventListener(
        "DOMContentLoaded", () => {
            const menu = new MmenuLight(
                document.querySelector( "#menu" )
            );

            const navigator = menu.navigation();
            const drawer = menu.offcanvas();

            document.querySelector( 'a[href="#menu"]' )
                .addEventListener( 'click', ( evnt ) => {
                    evnt.preventDefault();
                    drawer.open();
                });
        }
    );

module.exports = function(grunt) {
const sass = require('node-sass');
require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';',
      },
      scripts: {
        src: ['src/js/lazyload.js', 'src/js/jquery.ajaxchimp.js','src/js/owl.carousel.js', 'src/js/modal.js', 'src/js/lity.js', 'src/js/mmenu-light.polyfills.js', 'src/js/mmenu-light.js', 'src/js/app.js'],
        dest: 'dist/js/app.js',
      },
    },
    uglify: {
      my_target: {
        files: {
          'dist/js/app.min.js': ['dist/js/app.js']
        }
      }
    },
    connect: {
      server: {
        options: {
          port: 9000,
          base: '.',
          hostname: '0.0.0.0',
          protocol: 'http',
          livereload: true,
          open: true,
        }
      }
    },
    uncss: {
        dist: {
            files: {
                'dist/css/app-tidy.css': ['index.html']
            }
        }
    },
    sass: {
      options: {
        implementation: sass,
        sourceMap: false,
      },
      dist: {
        files: {
            'dist/css/app.css': 'src/app.sass'
        }
      }
    },
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          'dist/css/app.min.css': ['dist/css/app.css']
        }
      }
    },
    watch: {
      options: {
        livereload: true,
      },
      mainStyle: {
        files: ['src/*/*.sass', 'src/*/*.scss'],
        tasks: ['sass', 'remfallback', 'autoprefixer','csscomb', 'cssmin'],
      },
      mainHTML: {
        files: ['*.html'],
      },
      mainFont: {
        files: ['dist/glyphs/*.svg', 'src/wpcom/_Fonts.scss', 'src/wpcom/_wpcom-icons.scss'],
        tasks: ['webfont'],
      },
      mainJS: {
        files: ['src/js/*.js'],
        tasks: ['concat', 'uglify'],
      },
    },
    imagemin: {
      dynamic: {
          options: {
            optimizationLevel: 7,
          },
          files: [{
              expand: true,
              cwd: 'dist/img',
              src: ['*.{png,jpg,gif}'],
              dest: 'dist/img/min'
          }]
      }
    },
    webfont: {
      icons: {
          src: 'dist/glyphs/*.svg',
          dest: 'dist/fonts/webfont',
          destCss: 'src/wpcom',
          options: {
            font: 'wpcom-icons',
            fontFilename: 'wpcom',
            types: 'eot,woff2,woff,ttf,svg',
            order: 'woff2,eot,woff,ttf,svg',
            relativeFontPath: '../fonts/webfont',
            stylesheet: 'scss',
            htmlDemo: false,
          },
      }
    },
    autoprefixer:{
      options: {
        browsers: ['last 10 versions', 'ie 11', 'ie 10']
      },
      dist:{
        files:{
          'dist/css/app.css':'dist/css/app.css'
        }
      }
    },
    remfallback: {
      options: {
        log: false,
        replace: true
      },
      your_target: {
        files: {
          'dist/css/app.css': ['dist/css/app.css']
        }
      }
    },
    csscomb: {
      dist: {
        files: {
          'dist/css/app-resorted.css': ['dist/css/app.css']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-express');
  grunt.loadNpmTasks('grunt-parallel');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-uncss');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-webfont');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-remfallback');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-csscomb');

  // Default task(s).\
  grunt.registerTask('default', ['watch']);
  grunt.registerTask('css', ['sass', 'remfallback', 'autoprefixer', 'cssmin']);
  grunt.registerTask('server', ['connect','watch']);
  grunt.registerTask('font', ['webfont']);
  grunt.registerTask('production', ['sass', 'cssmin', 'imagemin', 'fontfactory']);


};

<?php
/**
 * 	Archive Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */


global $wp_query;
$cat = get_queried_object();
$term = $cat->term_id;
$cat_name = $cat->name;
$parent_cat = get_term_parents_list( $term, 'resource_category', $args = array('separator' => ''));

?>

<?php
if (get_queried_object()->parent == 234) {
	$post_fix = 'Plugins';
} elseif (get_queried_object()->term_id == 315 || get_queried_object()->term_id == 311) {
	$post_fix = 'Themes';
} else {
  $post_fix = "";
}
?>

<?php get_header('cat'); ?>


<?php
  $document_title = wp_get_document_title();
  $section_title = strtr($document_title, array("WPCOM"=>""));
  $section_title = get_queried_object()->name;
 ?>

<div class="content">
  <div class="container">
    <div class="row row-wrap">
      <!-- Sidebar Starts -->
      <?php get_template_part( 'templates/components/side-nav'); ?>
      <!-- Sidebar Ends -->
      <!-- Home Sections Starts -->
      <div id="archive" class="column column-75">
        <div class="row row-wrap">
          <div class="column column--section column-featured column-100">
            <div class="row row-wrap">
              <div class="column column-100">
                <div class="resource-loop">
                  <div class="section-title title--loop">
                    <h2>Best WordPress <?php echo $section_title . " " . $post_fix ?> (<?php echo $cat->count; ?>)</h2>
                  </div>
                  <?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="10" offset="0"  pause="true" posts_per_page="6" nested="true" post_type="resource" taxonomy="'. $cat->taxonomy .'" taxonomy_terms="'. $cat->slug .'" taxonomy_operator="IN" meta_key="resource_order" meta_value="" meta_compare="IN" orderby="meta_value_num" scroll="false" button_label="More Resources" button_loading_label="Loading Resources..."]'); ?>
                </div>
                <?php if(category_description($term)): ?>
                  <div class="catdesc-box">
                    <div class="section-title title--loop">
                      <h3>Top WordPress <?php echo $section_title ?></h3>
                    </div>
                    <div class="cat-desc">
                      <?php echo category_description($term); ?>
                    </div>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Home Sections Ends -->
  <?php get_template_part( 'templates/components/page-sections'); ?>
</div>

<?php get_footer(); ?>

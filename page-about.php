<?php
/* Template Name: About Us */
/**
 * 	Front Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */

?>
<?php get_header('page'); ?>



<div id="about" class="content">
  <div class="container">
    <div class="row row-wrap">
      <div id="about-content" class="column column-75">
        <div class="row row-wrap">
          <div class="column column-100">
            <div class="about-section">
              <div class="about-section-title">
                <h5>Who We Are</h5>
              </div>
              <div class="about-content about-page">
                <p>We are a small team of (string)3 developers who have speficially experienced on WordPress. All of us are spefically develop websites using WordPress as a Content Management System for a very long time. We are started with one simple idea and working together to curate a WordPress specific directory. At the end, we designed, developed and curated this directory while letting our years of experiences leads us.</p>
                <?php echo do_shortcode('[comitem rid="1358"]'); ?>
              </div>
            </div>
            <div class="about-section">
              <div class="about-section-title">
                <h5>How WPCOM was born</h5>
              </div>
              <div class="about-content about-page">
                <p>WPCOM is born with one simple idea; discovering all of the WordPress resources in one place -- to save time and have a bigger perspective about them. While doing this, we have curated some of the resources according to our previous experiences as well as adding the other possible alternatives, so our followers can share and rate their experienes to lead others.</p>
                <h5>Here are the <span class="problem">three problems</span> that we've followed to solve while developing this directory:</h5>
                <ul>
                  <li>There are tens of thousands of resources (Officially 47,350 active plugins on WordPress -- just the count of free plugins) and lot's of choices even for the small things like GDPR Notice Plugins etc.</li>
                  <li>Those resources listed on hundreds of different platforms and this requires a lot of time to dicover all of them and choose the right one without seeing the biased-reviews.</li>
                  <li>Most of the directories or the blogs are talking about how you can build, or optimize your WordPress website but they are rarely point out the places where you can improve your WordPress skills or discovering services for maintaining your website.</li>
                </ul>
                <h5>How <span class="solution">WPCOM solves</span> these problems:</h5>
                <li>WPCOM is a cureated directory with the "best parts of the iceberg" slogan. Seekers can discover one of the most reputable solutions for any kind of requirement as well as see the other alternatives.</li>
                <li>WPCOM is the one of a kind platform that you can decide how you can build your website or how you can improve your website in a very short time. Simply follow the category order and choose how you want to proceed. You'll see the all the alternatives for your specific requirement.</li>
                <li>You find the Tools, Hosting and Themes, now what? It's time to decide how you want to start. Developing your website by yourself with the resources in our Courses section or using one of the services in our Support Section.</li>
                <p>As a results of solving these problems, we hope that you can benefit all of the features of this directory and you can build your WordPress with less pain.</p>
              </div>
            </div>
            <div class="about-section">
              <div class="about-section-title">
                <h5>Leave us a Feedback</h5>
              </div>
              <div class="about-content about-page">
                <p style="margin-bottom: 15px;">This platform built for you and for a one major result; you'll find the resources you need faster. So, we are here to listen you. Please let us know how we can improve our platform further. Leave us a message! :)</p>
                <a href="<?php echo get_home_url();?>/contact">Go to Contact Page</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php get_template_part( 'templates/components/side-nav'); ?>
    </div>
  </div>
  <?php get_template_part( 'templates/components/page-sections'); ?>
</div>
<?php get_footer(); ?>

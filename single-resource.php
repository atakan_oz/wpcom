<?php
/**
 * 	Single Resources
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */

 global $s3_preview_bucket;
 global $cf_preview;

// Get Pod Fields
$pod = pods( 'resource', get_the_id());

// Retrieve Titles
$res_title = strtolower(get_the_title(get_the_id()));
$res_image_title = strtolower(str_replace(' ', '', get_the_title(get_the_id())));

// Retrieve Slug
$res_slug = get_post_field( 'post_name', get_the_id());

// Define Featured Title for Screenshot
$featured_title = $res_slug . "-ss";

// Define Icon Title for Screenshot
$icon_title = $res_slug . "-icon";

$utm = '';
$post_id = get_the_ID();

// Featured Image
if (has_post_thumbnail($post->ID)) {
  $featured_image = get_the_post_thumbnail_url();
} elseif ($pod->display('icon') === 'Yes'){
  $featured_image = "https://d27tr3630ifmlb.cloudfront.net/icons/" . $icon_title . ".png";
} elseif ($pod->display('website_preview')) {
  //$featured_image = "https://api.urlbox.io/v1/r8XtlBJip5dStJx1/jpg?use_s3=true&s3_bucket=wpcom.org&s3_path=%2Fscreenshots%2F". $featured_title ."&url=" . $pod->display('website_preview') . "&thumb_width=300&quality=80&ttl=2592000&width=1550&delay=3000&force=true";
  $featured_image = "https://d27tr3630ifmlb.cloudfront.net/screenshots/" . $featured_title . ".jpg";
} elseif($pod->display('theme_preview')) {
  $featured_image = $cf_preview . $res_image_title . ".jpg";
} else {
  //$featured_image = "https://api.urlbox.io/v1/r8XtlBJip5dStJx1/jpg?use_s3=true&s3_bucket=wpcom.org&s3_path=%2Fscreenshots%2F". $featured_title ."&url=" . $pod->display('website_link') . "&thumb_width=300&quality=80&ttl=2592000&width=1550&delay=3000&force=true";
  $featured_image = "https://d27tr3630ifmlb.cloudfront.net/screenshots/" . $featured_title . ".jpg";
}



// Retrieve Categories
$categories = get_the_terms( $post->ID, 'resource_category' );
$first_category = (!empty( $categories[0])) ? true : true;
$second_category = (!empty( $categories[1])) ? true : true;

// Retrieve Tags
$tags = get_the_terms( $post->ID, 'resource_tag' );

// Retrieve Post Link
$resource_link = $pod->display('website_link');

// Screenshot 1
$ss1 = $pod->display('website_preview');
$ss1_title = $res_slug . '-ss';
//$ss1_url = "https://api.urlbox.io/v1/r8XtlBJip5dStJx1/jpg?use_s3=true&s3_bucket=wpcom.org&s3_path=%2Fscreenshots%2F". $ss1_title ."&url=" . $ss1. "&thumb_width=1200&quality=80&ttl=2592000&width=1550&delay=3000&force=true";
$ss1_url = "https://d27tr3630ifmlb.cloudfront.net/screenshots/" . $ss1_title . ".jpg";

$selected_class = $pod->display('selected_resource');
$selected_text = ($pod->display('selected_resource') === 1) ? true : 'This is a selected resource.';

$pricing_info_text = $pod->display('pricing_info');
$deals = $pod->display('deals');

if($pod->display('resource_type') === 'Theme'){
  $post_fix = 'WordPress Theme';
}else {
  $post_fix = "";
}

// Retrieve Selected & Selected Class
$selected = $pod->display('post_selected', get_the_id());
if($selected === "Yes") {
  $selected_class = 'selected-resource';
} else {
    $selected_class = '';
}

$crew_picks_args = array(
  'post_type' => 'resource',
  'order' => 'DESC',
  'posts_per_page' => 10,
  'no_found_rows' => true,
  'post_status' => 'publish',
  'post__not_in' => array($post_id),
  'tax_query' => array(
    array(
        'taxonomy' => 'resource_tag',                //(string) - Taxonomy.
        'field' => 'id',                    //(string) - Select taxonomy term by ('id' or 'slug'
        'terms' => $tags[0]->term_id,
        'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
        'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
      ),
    ),
);


$related = array(
  'post_type' => 'resource',
  'order' => 'DESC',
  'posts_per_page' => 5,
  'no_found_rows' => true,
  'post_status' => 'publish',
  'post__not_in' => array($post_id),
  'tax_query' => array(
    array(
        'taxonomy' => 'resource_category',                //(string) - Taxonomy.
        'field' => 'id',                    //(string) - Select taxonomy term by ('id' or 'slug'
        'terms' => $categories[0]->term_id,
        'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
        'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
      ),
    ),
);




get_header();
?>
<div class="nav">
  <div class="container">
    <div class="row row-wrap">
      <div class="column column-100">
        <div class="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">
            <?php
            if(function_exists('bcn_display'))
            {
                    bcn_display();
            }?>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <div class="row row-wrap">
      <!-- Sidebar Starts -->
       <?php //get_template_part( 'templates/components/side-single'); ?>
      <!-- Sidebar Ends -->
      <div id="single-main" class="column column-100">
        <div class="row row-wrap">
            <div class="column column-100">
              <div class="row row-wrap">
                <div class="column column-featured column-100">
                  <div class="row row-wrap" itemscope itemtype="http://schema.org/WebApplication">
                    <div class="column column-75 column--single">
                      <div id="single-container" class="column column-100">
                        <?php
                        if (have_posts()) : ?>
                          <?php while (have_posts() ) : the_post(); ?>
                            <div id="<?php the_ID(); ?>" class="resource-card">

                              <?php if(function_exists('wp_ulike')) wp_ulike('get'); ?>
                              <div class="resource-link">
                                <div class="column rsc-img lazy" data-bg="url(<?php echo $featured_image ?>)"></div>
                                <div class="column rsc-content rsc-content">
                                  <h1 itemprop="name" class="rsc-title <?php echo $selected_class ?>"><?php the_title(); ?></h1>
                                  <div class="rsc-info">
                                    <div class="rsc-cat">
                                      <?php
                                      if($categories) {
                                        foreach($categories as $category) {
                                          echo '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '"><i class="icon icon_cat"></i><span itemprop="applicationCategory">' . esc_html( $category->name ) . '</span></a>';
                                        }
                                      }
                                      ?>
                                    </div>
                                  </div>
                                  <h2 itemprop="description" class="rsc-desc"><?php echo wp_trim_words(get_the_content(), 45, '...' ); ?></h2>
                                  <div class="rsc-links">
                                    <a data-micromodal-trigger="modal--share" itemprop="url" rel="nofollow" target="_blank" href="<?php echo $resource_link ?>"><i class="icon icon_link"></i> Go to Website</a>
                                  </div>
                								  <div style="display: none;" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                									  <span itemprop="bestRating">5</span>
                									  <span itemprop="worstRating">1</span>
                								   <span itemprop="ratingValue">4</span>
                								   <span itemprop="reviewCount">1</span>
                								  </div>
                                </div>
                              </div>
                              <div class="rsc-col-holder">
                                <div class="rsc-deals rsc-col">
                                  <span class="rsc-section-title">Deals</span>
                                  <?php if($pod->display('deal_code')): ?>
                                    <p><span class="coupon-code"><?php echo $pod->display('deal_code') ?></span> <?php echo $pod->display('deal_info') ?></p>
                                  <?php else: ?>
                                    <p>No data available.</p>
                                  <?php endif; ?>
                                </div>
                                <div class="rsc-tags rsc-col">
                                  <span class="rsc-section-title">Pricing Info</span>
                                  <?php if($pricing_info_text):?>
                                    <p><?php echo $pricing_info_text ?></p>
                                  <?php else: ?>
                                    <p>No data available.</p>
                                  <?php endif;?>
                                </div>
                                <div class="rsc-tags rsc-col">
                                  <span class="rsc-section-title">Listed In</span>
                                  <?php
                                  if(!empty($tags)){
                                    foreach($tags as $tag){
                                      echo '<a class="rsc-tag" href="' . esc_url( get_tag_link( $tag->term_id ) ) . '">' . esc_html( $tag->name ) . '</a>';
                                    }
                                  }
                                  ?>
                                </div>
                              </div>
                              <div style="display: none">
                                <?php if($pod->display('theme_preview')): ?>
                                  <div class="single-ss theme-preview">
                                    <div class="rsc-ss-item"><a href="<?php echo $featured_image ?>" data-lity data-lity-target="<?php echo $featured_image ?>"><img src="<?php echo $featured_image ?>" alt=""></a></div>
                                    <div class="rsc-source">
                                      <div class="source source--envato">
                                        <div class="source-content">
                                          <span>On</span>
                                          <img src="<?php echo get_template_directory_uri() ?>/dist/img/envatomarket.png" alt="theme on envato market">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                <?php else: ?>
                                  <div class="single-ss">
                                    <div class="rsc-ss-item"><a href="<?php echo $ss1_url ?>" data-lity data-lity-target="<?php echo $ss1_url ?>"><img itemprop="image" src="<?php echo $ss1_url ?>" alt="<?php the_title(); ?> reviews and alternatives"></a></div>
                                  </div>
                                <?php endif; ?>
                              </div>
                            </div>
                          <?php endwhile;?>
                        <?php else : ?>
                        <?php endif; wp_reset_postdata();?>
                      </div>
                    </div>
                    <div class="column column-25 column--single">
                      <div class="row row-wrap">
                        <div class="column column-100">
                          <div class="resource-loop loop--single">
                            <div class="section-title title--loop title--featured">
                              <h5>Who Viewed This Item Also Viewed</h5>
                              <p style="margin-top: 8px">Other visitors who visited the current resources also interested in these resources:</p>
                            </div>
                            <div class="column column-100 owl-carousel owl-theme owl-featured-single featured-style owl-loaded owl-drag">
                              <?php
                              $featured_query2 = new WP_Query($crew_picks_args);
                              if ($featured_query2->have_posts()) : ?>
                                <?php while ( $featured_query2->have_posts() ) : $featured_query2->the_post(); ?>
                                  <?php get_template_part('/templates/posts/featured'); ?>
                                <?php endwhile;?>
                              <?php else : ?>
                              <?php endif; wp_reset_query();?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="column column-100 column--single ">
                      <div class="row row-wrap">
                        <div class="column column-100">
                          <?php comments_template(); ?>
                        </div>
                      </div>
                    </div>
                    <div class="column column-100 column--single">
                      <div class="row row-wrap">
                        <div class="column column-100">
                          <div class="resource-loop">
                            <div class="section-title title--loop">
                              <h5>More Resource In This Category: <?php echo $categories[0]->name ?></h5>
                            </div>
                            <div class="column column-100">
                              <?php
                              $featured_query3 = new WP_Query($related);
                              if ($featured_query3->have_posts()) : ?>
                                <?php while ( $featured_query3->have_posts() ) : $featured_query3->the_post(); ?>
                                  <?php get_template_part('/templates/posts/standard'); ?>
                                <?php endwhile;?>
                              <?php else : ?>
                              <?php endif; wp_reset_query();?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Home Sections Ends -->
  <?php get_template_part( 'templates/components/page-sections'); ?>
</div>
<script>
console.log('<?php echo $selected_text ?>');
</script>
<?php get_footer(); ?>

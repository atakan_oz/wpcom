<?php
/* Template Name: Hire */
/**
 * 	Front Page
 *
 * 	@author      WPCOM
 * 	@package     wpcom
 * 	@version     1.0
 *
 */

?>
<?php get_header('page'); ?>

<div id="about" class="content">
  <div class="container">
    <div class="row row-wrap">
      <div id="about-content" class="column column-75">
        <div class="row row-wrap">
          <div class="column column-100">
            <div class="about-section">
              <div class="about-section-title">
                <h5>Hire WP Experts</h5>
              </div>
              <div id="codeable-form" class="about-content">
              </div>
              <script>
                (function(c,o,d,e,a,b,l){c['CodeableObject']=a;c[a]=c[a]||function(){
                (c[a].q=c[a].q||[]).push(arguments)},c[a].l=1*new Date();b=o.createElement(d),
                l=o.getElementsByTagName(d)[0];b.async=1;b.src=e;l.parentNode.insertBefore(b,l)
                })(window,document,'script','https://referoo.co/assets/form.js','cdbl');

                cdbl('shortcode', '5JTEe');
                cdbl('render', 'codeable-form');
              </script>
            </div>
          </div>
        </div>
      </div>
      <?php get_template_part( 'templates/components/side-nav'); ?>
    </div>
  </div>
  <?php get_template_part( 'templates/components/page-sections'); ?>
</div>
<?php get_footer(); ?>
